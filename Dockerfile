# ubuntu-rust-test-v

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=0.1
ARG UBUNTU_VERSION=18.04
ARG RUST_VERSION=1.38.0


FROM ${DOCKER_REGISTRY_URL}ubuntu-rust:${CUSTOM_VERSION}-${UBUNTU_VERSION}-${RUST_VERSION} AS build

WORKDIR /app

VOLUME /app

CMD cargo build --release


FROM ${DOCKER_REGISTRY_URL}ubuntu:${CUSTOM_VERSION}-${UBUNTU_VERSION} AS ubuntu-rust-test-v

WORKDIR /app

COPY ./target/release/ubuntu-rust-test /app/

CMD /app/ubuntu-rust-test
